<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 20/02/2019
 * Time: 11:00
 */

namespace App\Controller;


use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{
	/**
	 * @Route("/", name="homepage")
	 */
	public function index(ArticlesRepository $articlesRepository): Response
	{
		if( $this->getUser() && in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
			$token = $this->getUser()->getConfirmationToken();
			if( !$token){
				$token = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
				$user = $this->getUser();
				$user->setConfirmationToken($token);
				$this->getDoctrine()->getManager()->persist($user);
				$this->getDoctrine()->getManager()->flush();
			}
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
			$url .= $this->generateUrl('articles_get_new');
			$url .='?token='.$token.'&url=${url}&title=${title}&source=${source}&id=${id}&description=${description}&keywords=${keywords}&creator=${creator}&thumbnailurl=${thumbnailurl}';
			return $this->render( 'articles/index_admin.html.twig', ['url' => $url] );
		}else {
			return $this->redirectToRoute( 'fos_user_security_login', [], 301 );
		}
	}


	/**
	 * @Route("/feeds", name="feeds", methods={"GET"})
	 */
	public function feeds(ArticlesRepository $articlesRepository): Response
	{
		$response = new Response();
		$response->headers->set('Content-Type', 'application/xml; charset=utf-8');
		return $this->render('articles/index.xml.twig', [
			'articles' => $articlesRepository->findAll(),
		], $response);

	}



	/**
	 * @Route("/add", name="articles_get_new", methods={"GET"})
	 */
	public function newGet(Request $request,UserManagerInterface $user_manager): Response
	{
		$token = $request->query->get('token', null);
		$user = null;
		if( $token != null )
			$user = $user_manager->findUserBy(["confirmationToken" => $token]);
		if($user) {
			$article = new Articles();

			$title = $request->query->get('title', null);
			$url = $request->query->get('url', null);
			$source = $request->query->get('source', null);
			$id = $request->query->get('id', null);
			$description = $request->query->get('description', null);
			$keywords = urldecode($request->query->get('keywords', null));
			$guid = $request->query->get('id', null);
			$pubdate = $request->query->get('pubdate', null);
			$creator =  $request->query->get('creator', null);
			$category =  $request->query->get('category', null);
			$thumbnailUrl =  $request->query->get('thumbnailurl', null);
			if( !$title || !$description || !$url || !$id){
				throw new BadRequestHttpException('Title, description, url and id cannot be empty!', null, 400);
			}
			if(!$pubdate)
				$pubdate = new \DateTime();
			$article->setTitle($title);
			$article->setUser($user);
			$article->setTitle($title);
			$article->setLink($url);
			$article->setPubdate($pubdate);
			$article->setCreator($creator);
			$article->setCategory($category);
			$article->setDescription($description);
			$article->setThumbnailUrl($thumbnailUrl);
			if( $keywords ){
				$keywordArray = explode(" ", $keywords);
				$article->setKeywords($keywordArray);
			}
			$article->setGuid($guid);
			$article->setSource($source);
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist( $article );
			$entityManager->flush();
			$encoders = [new XmlEncoder(), new JsonEncoder()];
			$normalizers = [new ObjectNormalizer()];

			$serializer = new Serializer($normalizers, $encoders);
			$article->setUser(null);
			$response = new Response($serializer->serialize($article, 'json'));
			$response->headers->set('Content-Type', 'application/json');
			return $response;

		}else{
			throw $this->createAccessDeniedException('You cannot access this page!');
		}
	}



	/**
	 * @Route("/regenerate_token", name="regenerate_token", methods={"POST"})
	 */
	public function regenerate(Request $request): Response
	{

		if ($this->isCsrfTokenValid('regenerate', $request->request->get('_token'))) {
			$token = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
			$user = $this->getUser();
			$user->setConfirmationToken($token);
			$this->getDoctrine()->getManager()->persist($user);
			$this->getDoctrine()->getManager()->flush();
		}

		return $this->redirectToRoute('homepage');
	}
}