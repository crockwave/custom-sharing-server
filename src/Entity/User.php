<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 20/02/2019
 * Time: 10:26
 */

namespace App\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User  extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    protected $username;

    protected $password;

    protected $confirmationToken;

	public function __construct()
   	{
   		parent::__construct();
   		// your own logic
   	}

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPlainPassword(): ?string
    {
        return $this->password;
    }

    public function getConfirmationtoken(): ?string
    {
        return $this->confirmationToken;
    }
}