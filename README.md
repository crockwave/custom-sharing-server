### How to install

1. Clone the repository

2. In root directory edit `.env` file, and change with yours:

```
rss_feed_title="localhost RSS feeds"
rss_feed_url="http://localhost"
rss_feed_description="This is my awesome RSS feeds"
rss_feed_lang=en-us
rss_feed_copyright="Copyright (C) 2019 - Fedilab"
```

3. In the same file, change your db connection:

`DATABASE_URL=mysql://db_user:db_pwd@db_host:db_port/db_name`

Example:

`DATABASE_URL=mysql://fedilab_content_creator:fedilab_content_creator@127.0.0.1:8889/fedilab_content_creator`

4. Install vendors:

`composer install`

PS: You need to install `composer`, just use:

`apt install composer`

Or you can do that with the following commands:

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

See: [Download Composer](https://getcomposer.org/download/)

5. Initialize database:

`php bin/console doctrine:database:create`

`php bin/console doctrine:migrations:migrate`


6. Create a user for managing the app (change with your values):

`php bin/console fos:user:create testuser test@example.com p@ssword`

7. Grant admin role (change `testuser`):

`php bin/console fos:user:promote testuser ROLE_ADMIN`

[Requirements are ](https://symfony.com/doc/current/reference/requirements.html) :

8. Make sure that `var/log/` has write access otherwise you will get error 500.


### Some screenshots:

<img src="images/pic01.png" width="450"/>

<img src="images/pic02.png" width="450"/>

<img src="images/pic03.png" width="450"/>



> PHP version: 7.1.3 or higher
> PHP extensions: (all of them are installed and enabled by default in PHP 7+)
> Ctype
> iconv
> JSON
> PCRE
> Session
> SimpleXML
> Tokenizer
> Writable directories: (must be writable by the web server)
> The project's cache directory (var/cache/ by default, but the app can override the cache dir)
>  The project's log directory (var/log/ by default, but the app can override the logs dir)



